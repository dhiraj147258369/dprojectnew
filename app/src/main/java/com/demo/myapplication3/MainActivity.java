package com.demo.myapplication3;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Notification Channel
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel("My Notification","My notification", NotificationManager.IMPORTANCE_DEFAULT);
            NotificationManager manager = getSystemService(NotificationManager.class);
            manager.createNotificationChannel(channel);
        }

        //THIS IS DHIRAJ

        Button showbtn = findViewById(R.id.button);
        Button locationActivityBtn  = findViewById(R.id.location_activity);


        Intent  clickOnNotificationintent = new Intent(this,MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this,0,clickOnNotificationintent,0);

        Intent broadcastIntent = new Intent(this,NotificationReceiver.class);
        broadcastIntent.putExtra("toastMessage","This the new message from notification");

        PendingIntent actionIntent = PendingIntent.getBroadcast(this
               ,0,broadcastIntent,PendingIntent.FLAG_UPDATE_CURRENT);
        showbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //Notification Builder
                NotificationCompat.Builder builder = new NotificationCompat.Builder(
                        MainActivity.this,"My Notification");
                builder.setContentTitle("My title");
                builder.setContentText("Hi,this is dhiraj, how are you all?");
                builder.setColor(Color.argb(100, 233, 30, 99));
                builder.setSmallIcon(R.drawable.ic_launcher_background);
                builder.setPriority(NotificationCompat.PRIORITY_HIGH);
                builder.setCategory(NotificationCompat.CATEGORY_MESSAGE);
                builder.setContentIntent(pendingIntent);
                builder.addAction(R.mipmap.ic_launcher,"toast",actionIntent);
                builder.setAutoCancel(true);

                Log.e("My title","Clicked");

                //Notification Manager
                NotificationManagerCompat managerCompat = NotificationManagerCompat.from(MainActivity.this  );
                managerCompat.notify(1,builder.build());
            }
        });

        locationActivityBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this,MainActivity2.class);
                startActivity(intent);
            }
        });

    }
}